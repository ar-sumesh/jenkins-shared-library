package com.example

class Docker implements Serializable {

    def script
    Docker(script) {
        this.script = script
    }

    def buildImage(String imageName) {
        script.echo "building the docker image... $imageName"
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh 'docker build -t nanajanashia/demo-app:jma-2.0 .'
            script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin"
            script.sh 'docker push nanajanashia/demo-app:jma-2.0'
        }
    }
}
